#!/usr/bin/env python3

import numpy as np

import sys
import random

def random_SU_2():
    """ Generate a Haar-random SU(2) matrix. """
    alpha = random.gauss(0,1) + 1j*random.gauss(0,1)
    beta = random.gauss(0,1) + 1j*random.gauss(0,1)
    norm = np.sqrt(abs(alpha)**2 + abs(beta)**2)
    return np.array([[alpha, -beta.conjugate()], [beta, alpha.conjugate()]])/norm

def random_SU_N(N):
    """ Obtain an approximately Haar-random SU(N) unitary.
    """
    K = 5 * N * N
    U = np.eye(N)
    for k in range(K):
        i, j = random.sample(range(N), k=2)
        W = random_SU_2()
        V = np.eye(N) + 0j
        V[i,i] = W[0,0]
        V[i,j] = W[0,1]
        V[j,i] = W[1,0]
        V[j,j] = W[1,1]
        U = U @ V
    return U

Q = int(sys.argv[1])
U = random_SU_N(2**Q)
for i in range(2**Q):
    print('\t'.join(str(x) for x in U[i,:]))
