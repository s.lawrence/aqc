#!/bin/sh
set -e
dir=`dirname $0`

if [ ! -d $dir/haar ]; then
	mkdir -p $dir/haar
	for K in `seq 1 20`; do
		for Q in `seq 1 5`; do
			python3 $dir/gen-haar.py $Q > $dir/haar/${Q}qubit,$K
		done
	done
else
	echo "$dir/haar exists, skipping..."
fi

if [ ! -d $dir/circuit ]; then
	mkdir -p $dir/circuit
	for K in `seq 1 10`; do
		for Q in `seq 1 5`; do
			for G in `seq 10 10 100`; do
				python3 $dir/gen-circuit.py $Q $G > $dir/circuit/${Q}qubit,${G}gate,$K
			done
		done
	done
else
	echo "$dir/circuit exists, skipping..."
fi

if [ ! -d $dir/toffoli ]; then
	mkdir -p $dir/toffoli
	for n in `seq 2 10`; do
		python3 $dir/gen-toffoli.py $n > $dir/toffoli/toffoli$n
	done
else
	echo "$dir/toffoli exists, skipping..."
fi

if [ ! -d $dir/fourier ]; then
	mkdir -p $dir/fourier
else
	echo "$dir/fourier exists, skipping..."
fi
