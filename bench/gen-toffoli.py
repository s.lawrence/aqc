#!/usr/bin/env python3

import numpy as np

import sys

Q = int(sys.argv[1])

U = np.eye(2**Q)
U[2**Q-1,2**Q-1] = 0
U[2**Q-2,2**Q-2] = 0
U[2**Q-2,2**Q-1] = 1
U[2**Q-1,2**Q-2] = 1


for i in range(2**Q):
    print('\t'.join(str(x) for x in U[i,:]))

