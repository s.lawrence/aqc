#!/usr/bin/env python3

import numpy as np

import sys
import random

def gate(Q, args, V):
    assert V.shape == (2**len(args), 2**len(args))
    nargs = list(range(Q))
    for a in args:
        nargs.remove(a)
    gQ = len(args)
    oQ = Q - len(args)
    U = np.zeros((2**Q, 2**Q)) + 0j
    for i in range(2**gQ):
        for j in range(2**gQ):
            for k in range(2**oQ):
                x = 0
                y = 0
                for n in range(Q):
                    if n in args:
                        x += int(bool(i & (1 << args.index(n)))) * 2**n
                        y += int(bool(j & (1 << args.index(n)))) * 2**n
                    else:
                        x += int(bool(k & (1 << nargs.index(n)))) * 2**n
                        y += int(bool(k & (1 << nargs.index(n)))) * 2**n
                U[x, y] = V[i, j]
    return U

def hadamard_gate(Q, n):
    return gate(Q, [n], np.array([[1, 1], [1, -1]])/np.sqrt(2))

def pi8_gate(Q, n):
    return gate(Q, [n], np.array([[1, 0], [0, np.exp(1j*np.pi/4)]]))

def pi8inv_gate(Q, n):
    return gate(Q, [n], np.array([[1, 0], [0, np.exp(-1j*np.pi/4)]]))

def cx_gate(Q, n, m):
    cx = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]])
    return gate(Q, [n, m], cx)

def random_gate(Q):
    if random.random() < 0.4 and Q > 1:
        return cx_gate(Q, *random.sample(range(Q), k=2))
    else:
        fun = random.choice([hadamard_gate, pi8_gate, pi8inv_gate])
        return fun(Q, random.choice(range(Q)))
    return np.eye(2**Q) + 0j

def random_circuit(Q, G):
    U = np.eye(2**Q) + 0j
    for g in range(G):
        U = random_gate(Q) @ U
    return U

Q = int(sys.argv[1])
G = int(sys.argv[2])

U = random_circuit(Q, G)
for i in range(2**Q):
    print('\t'.join(str(x) for x in U[i,:]))

