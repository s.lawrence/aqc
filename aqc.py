#!/usr/bin/env python3

from copy import copy
from functools import reduce
import operator
import random

import numpy as np

# Utility functions for numpy matrix operations
def adj(M):
    return M.conj().T

# Distance functions for unitaries
def dist(U, V):
    assert U.shape == V.shape
    N = U.shape[0]
    return (N - (adj(U) @ V).trace().real)/N

def norm_distinct_eigenvalues(U, h=1e-6):
    eigv, _ = np.linalg.eig(U)
    N = U.shape[0]
    ndistinct = 0
    for i in range(N):
        distinct = True
        for j in range(i):
            if abs(eigv[i] - eigv[j]) < h:
                distinct = False
        if distinct:
            ndistinct += 1
    return (ndistinct-1) / N

def dist_distinct_eigenvalues(U, V):
    return norm_distinct_eigenvalues(adj(U)@V)

def p_adic_norm(x, p=10):
    exp = 0
    while x % p**exp == 0:
        exp += 1
    return p**(1-exp)

def norm_eigenvalue_multiplicities(U, h=1e-6):
    eigv, _ = np.linalg.eig(U)
    N = U.shape[0]
    mults = {}
    for v in eigv:
        dist = True
        for vp in mults:
            if dist and (abs(v - vp) < h):
                mults[vp] += 1
                dist = False
        if dist:
            mults[v] = 1
    r = 0.
    for m in mults.values():
        r += p_adic_norm(m, p=2)
    return r - 1/N

def dist_eigenvalue_multiplicities(U, V):
    return norm_eigenvalue_multiplicities(adj(U)@V)


def unitarize(U):
    N = U.shape[0]
    for i in range(N):
        for j in range(i):
            prod = adj(U[j,:]) @ U[i,:]
            U[i,:] = U[i,:] - prod * U[j,:]
        norm = adj(U[i,:]) @ U[i,:]
        U[i,:] = U[i,:] / np.sqrt(norm)
    return U

def random_unitary(N, delta):
    """ Produce a random N-by-N unitary.

    The unitary is not drawn from the Haar measure, but rather is concentrated
    about the identity.

    This method is quite slow. In practice, it's better to cache a large list
    of random unitaries, and simply select from that instead.
    """
    H = np.random.normal(size=(N, N)) + 1j * np.random.normal(size=(N, N))
    H = delta * (H + adj(H))/2
    U = np.zeros((N,N))
    Hk = np.eye(N)
    k = 0
    while np.sum(abs(Hk)) > 1e-6:
        U = U + Hk
        k = k+1
        Hk = Hk @ H / k
    return unitarize(U)

class UnitaryGenerator:
    def __init__(self, N, delta, K=100):
        self.N = N
        self.K = K
        self.delta = delta
        self._generate_cache()

    def _generate_cache(self):
        if self.delta > 1.:
            self.delta = 1.
        self.cache_delta = self.delta
        self.cache = []
        for k in range(self.K):
            self.cache.append(random_unitary(self.N, self.delta))

    def __call__(self):
        return random.choice(self.cache)


GATES = {}

def register_gate(name):
    def dec(gate):
        GATES[name] = gate
        return gate
    return dec

class Gate:
    def __init__(self, Q, args, V):
        self.args = args
        self.V = V + 0j
        assert V.shape == (2**len(args), 2**len(args))
        nargs = list(range(Q))
        for a in args:
            nargs.remove(a)
        gQ = len(args)
        oQ = Q - len(args)
        U = np.zeros((2**Q, 2**Q)) + 0j
        for i in range(2**gQ):
            for j in range(2**gQ):
                for k in range(2**oQ):
                    x = 0
                    y = 0
                    for n in range(Q):
                        if n in args:
                            x += int(bool(i & (1 << args.index(n)))) * 2**n
                            y += int(bool(j & (1 << args.index(n)))) * 2**n
                        else:
                            x += int(bool(k & (1 << nargs.index(n)))) * 2**n
                            y += int(bool(k & (1 << nargs.index(n)))) * 2**n
                    U[x, y] = V[i, j]
        self.U = U

    def _params(self):
        return {}

    def _name(self):
        return '(?)'

    def _desc(self):
        return self._name()

    def __str__(self):
        return F'{self._desc()}\t'+'\t'.join(str(a) for a in self.args)

@register_gate("I")
class IGate(Gate):
    N = 0
    def __init__(self, Q, rand=False):
        Gate.__init__(self, Q, [],  np.array([[1]]))

    def _name(self):
        return 'I'

@register_gate("X")
class XGate(Gate):
    N = 1
    def __init__(self, Q, n, rand=False):
        Gate.__init__(self, Q, [n], np.array([[0, 1], [1, 0]]))

    def _name(self):
        return 'X'

@register_gate("Y")
class YGate(Gate):
    N = 1
    def __init__(self, Q, n, rand=False):
        Gate.__init__(self, Q, [n], np.array([[0, -1j], [1j, 0]]))

    def _name(self):
        return 'Y'

@register_gate("Z")
class ZGate(Gate):
    N = 1
    def __init__(self, Q, n, rand=False):
        Gate.__init__(self, Q, [n], np.array([[1, 0], [0, -1]]))

    def _name(self):
        return 'Z'

@register_gate("H")
class HadamardGate(Gate):
    N = 1
    def __init__(self, Q, n, rand=False):
        Gate.__init__(self, Q, [n], np.array([[1, 1], [1, -1]])/np.sqrt(2))

    def _name(self):
        return 'H'

@register_gate("T")
class Pi8Gate(Gate):
    N = 1
    def __init__(self, Q, n, rand=False):
        Gate.__init__(self, Q, [n], np.array([[1, 0], [0, np.exp(1j*np.pi/4)]]))

    def _name(self):
        return 'T'

@register_gate("Tdag")
class Pi8InvGate(Gate):
    N = 1
    def __init__(self, Q, n, rand=False):
        Gate.__init__(self, Q, [n], np.array([[1, 0], [0, np.exp(-1j*np.pi/4)]]))

    def _name(self):
        return 'Tdag'

@register_gate("CX")
class CXGate(Gate):
    N = 2
    def __init__(self, Q, tgt, ctl, rand=False):
        cx = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]])
        Gate.__init__(self, Q, [tgt, ctl], cx)

    def _name(self):
        return 'CX'

@register_gate("CY")
class CYGate(Gate):
    N = 2
    def __init__(self, Q, tgt, ctl, rand=False):
        cx = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, -1j], [0, 0, 1j, 0]])
        Gate.__init__(self, Q, [tgt, ctl], cx)

    def _name(self):
        return 'CY'

@register_gate("CZ")
class CZGate(Gate):
    N = 2
    def __init__(self, Q, tgt, ctl, rand=False):
        cx = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, -1]])
        Gate.__init__(self, Q, [tgt, ctl], cx)

    def _name(self):
        return 'CZ'

class ContinuousGate(Gate):
    """ Base class for gates that allow continuous parameters. """
    def _desc(self):
        return F'{self._name()} {self._params()}'

@register_gate("RX")
class RXGate(ContinuousGate):
    N = 1
    def __init__(self, Q, n, theta=0., rand=False):
        if rand:
            theta = random.uniform(0,2*np.pi)
        self.theta = theta
        V = np.array([[np.cos(theta), 1j*np.sin(theta)], [-1j*np.sin(theta), np.cos(theta)]])
        Gate.__init__(self, Q, [n], V)

    def _params(self):
        return {'theta': self.theta}

    def _name(self):
        return 'RX'

@register_gate("RY")
class RYGate(ContinuousGate):
    N = 1
    def __init__(self, Q, n, theta=0., rand=False):
        if rand:
            theta = random.uniform(0,2*np.pi)
        self.theta = theta
        V = np.array([[np.cos(theta), np.sin(theta)], [-np.sin(theta), np.cos(theta)]])
        Gate.__init__(self, Q, [n], V)

    def _params(self):
        return {'theta': self.theta}

    def _name(self):
        return 'RY'

@register_gate("RZ")
class RZGate(ContinuousGate):
    N = 1
    def __init__(self, Q, n, theta=0., rand=False):
        if rand:
            theta = random.uniform(0,2*np.pi)
        self.theta = theta
        Gate.__init__(self, Q, [n], np.array([[1, 0], [0, np.exp(-1j*theta)]]))

    def _params(self):
        return {'theta': self.theta}

    def _name(self):
        return 'RZ'

@register_gate("RZZ")
class RZZGate(ContinuousGate):
    N = 2
    def __init__(self, Q, n, m, theta=0., rand=False):
        if rand:
            theta = random.uniform(0,2*np.pi)
        self.theta = theta
        V = np.array([[np.exp(1j*theta), 0, 0, 0],
                      [0, 1, 0, 0],
                      [0, 0, 1, 0],
                      [0, 0, 0, np.exp(1j*theta)]])
        Gate.__init__(self, Q, [n, m], V)

    def _params(self):
        return {'theta': self.theta}

    def _name(self):
        return 'RZZ'


@register_gate("SU2")
class OneQubitGate(ContinuousGate):
    N = 1
    def __init__(self, Q, n, alpha=0., beta=0., gamma=0., rand=False):
        if rand:
            # TODO this isn't actually the right distribution
            alpha = random.gauss(0, 200)
            beta = random.gauss(0, 200)
            gama = random.gauss(0, 200)
        self.alpha = alpha % (2*np.pi)
        self.beta = beta % (2*np.pi)
        self.gamma = gamma % (2*np.pi)
        x = np.cos(alpha) * np.exp(1j*beta)
        y = np.sin(alpha) * np.exp(1j*gamma)
        V = np.array([[x, -y.conj()], [y, x.conj()]])
        Gate.__init__(self, Q, [n], V)

    def _params(self):
        return {'alpha': self.alpha, 'beta': self.beta, 'gamma': self.gamma}

    def _name(self):
        return 'SU2'

class Circuit:
    def __init__(self, Q, G):
        self.Q = Q
        self.gates = [IGate(Q)]*G

    def __len__(self):
        return len(self.gates)

    def __getitem__(self, i):
        return self.gates[i]

    def __setitem__(self, i, g):
        self.gates[i] = g

    def insert(self, i, g):
        self.gates.insert(i, g)

    def __delitem__(self, i):
        del self.gates[i]

    def __copy__(self):
        cp = Circuit(self.Q, 0)
        cp.gates = copy(self.gates)
        return cp

    def unitary(self):
        if len(self.gates) == 0:
            return np.eye(2**Q)
        return reduce(operator.matmul, (g.U for g in self.gates))

    def __str__(self):
        gs = '\n'.join(str(g) for g in self.gates)
        return F'Circuit on {self.Q} qubits\n{gs}'

class Move:
    def __call__(self, obj):
        new = copy(obj)
        cb = self.move(new)
        return new, cb

def _random_gate(gates, Q):
    gate = random.choice(list(gates))
    while gate.N > Q:
        gate = random.choice(list(gates))
    qubits = random.sample(range(Q), k=gate.N)
    return gate(Q, *qubits, rand=True)

class ReplaceMove(Move):
    def __init__(self, gates):
        self.gates = gates

    def move(self, circuit):
        if len(circuit) == 0:
            return lambda _: None
        # Number to replace
        n = int(random.expovariate(1.)+1)
        if n > len(circuit):
            n = len(circuit)
        i = random.choice(range(len(circuit) - n + 1))
        for k in range(i,i+n):
            circuit[k] = _random_gate(self.gates, circuit.Q)
        return lambda _: None

class SwapMove(Move):
    def move(self, circ):
        ids = range(len(circ))
        if len(ids) < 2:
            return lambda _: None
        i, j = random.choice([(x,y) for x in ids for y in ids if x < y])
        circ[i], circ[j] = circ[j], circ[i]
        return lambda _: None

class PermuteMove(Move):
    def move(self, circ):
        ids = range(len(circ))
        if len(ids) < 2:
            return lambda _: None
        lo, hi = random.choice([(x,y) for x in ids for y in ids if x < y])
        mid = random.choice(range(lo, hi))
        midp = hi - mid + lo - 1
        circ[lo:midp+1], circ[midp+1:hi+1] = circ[mid+1:hi+1], circ[lo:mid+1]
        return lambda _: None

class AlterGateMove(Move):
    def __init__(self):
        self.delta = {}

    def _callback(self, typ):
        def _cb(acc):
            if acc:
                self.delta[typ] *= 1.01
            else:
                self.delta[typ] /= 1.01
        return _cb

    def __copy__(self):
        cp = AlterGateMove()
        cp.delta = copy(self.delta)
        return cp

    def move(self, circuit):
        if len(circuit) == 0:
            return lambda _: None
        i = random.choice(range(len(circuit)))
        typ = type(circuit[i])
        if typ not in self.delta:
            self.delta[typ] = 0.3
        delt = self.delta[typ]

        params = circuit[i]._params()
        for k in params:
            params[k] += random.gauss(0, delt)
        circuit[i] = typ(circuit.Q, *circuit[i].args, **params)

        return self._callback(typ)

class CircuitMoveset:
    """ Moveset for a general circuit. """
    def __init__(self, gates):
        self.gates = gates
        self.moves = [ReplaceMove(gates),
                      SwapMove(),
                      PermuteMove(),
                      AlterGateMove(),
                     ]

    def __call__(self, circuit):
        return random.choice(self.moves)(circuit)

    def __copy__(self):
        cp = CircuitMoveset(self.gates)
        # TODO
        return cp

def commutator(U, V):
    return U @ V - V @ U

class Reduction:
    """ Expressing a unitary as a product of many (perhaps simpler) unitaries.

    This is an alternative to the `Circuit` framework. There, one fixes the set
    of gates in advance, and seeks to get close to a goal unitary within that
    set of gates. Here, one fixes the goal, and searches for a decomposition
    with nice properties.
    """

    def __init__(self, Q, L, goal):
        self.Q = Q
        self.L = L
        self.goal = goal
        self.U = [np.eye(2**Q) for _ in range(L)]
        self.U[int(L/2)] = goal
        self.sigma_x = [np.eye(1)+0j]*Q
        self.sigma_y = [np.eye(1)+0j]*Q
        self.sigma_z = [np.eye(1)+0j]*Q
        for i in range(Q):
            for j in range(Q):
                if i == j:
                    self.sigma_x[i] = np.kron(np.array([[0, 1], [1, 0]]), self.sigma_x[i])
                    self.sigma_y[i] = np.kron(np.array([[0, -1j], [1j, 0]]), self.sigma_y[i])
                    self.sigma_z[i] = np.kron(np.array([[1, 0], [0, -1]]), self.sigma_z[i])
                else:
                    self.sigma_x[i] = np.kron(np.eye(2), self.sigma_x[i])
                    self.sigma_y[i] = np.kron(np.eye(2), self.sigma_y[i])
                    self.sigma_z[i] = np.kron(np.eye(2), self.sigma_z[i])

    def score(self):
        score = 0
        for V in self.U:
            eff = 0
            for q in range(self.Q):
                comx = np.sum(abs(commutator(self.sigma_x[q], V)))
                comy = np.sum(abs(commutator(self.sigma_y[q], V)))
                comz = np.sum(abs(commutator(self.sigma_z[q], V)))
                com = comx**2 + comy**2 + comz**2
                eff += com**2
            score += eff
        return score

    def __len__(self):
        return self.L

    def __str__(self):
        return '\t'.join([str(U) for U in self.U])

    def __copy__(self):
        cp = Reduction(self.Q, self.L, self.goal)
        cp.U = copy(self.U)
        return cp

    def unitary(self):
        return reduce(operator.matmul, self.U)

class ReductionMove(Move):
    def __init__(self, Q):
        self.Q = Q
        self.delta = 0.0002
        self.accepted = 0
        self.proposed = 0
        self.generator = UnitaryGenerator(2**Q, self.delta)

    def __copy__(self):
        cp = ReductionMove(self.Q)
        return cp

    def __accepted(self, acc):
        self.proposed += 1
        if acc:
            self.accepted += 1
        if self.proposed > 100:
            if self.accepted < 30:
                self.delta /= 1.01
            elif self.accepted > 50:
                self.delta *= 1.01
            self.proposed = 0
            self.accepted = 0
        self.generator.delta = self.delta

    def move(self, red):
        i = random.choice(range(len(red)-1))
        j = i+1
        V = self.generator()
        red.U[i] = red.U[i] @ adj(V)
        red.U[j] = V @ red.U[j]
        return self.__accepted


class Annealing:
    """ Markov-Chain Monte Carlo
    """

    def __init__(self, initial, moves, schedule, score=None, K=10):
        self.K = K
        self.state = initial
        self.moves = moves
        self.scorefun = score
        self.stepi = 0
        self.schedule = schedule
        self.temperature = self.schedule(0)

    def score(self, obj=None):
        if obj is None:
            obj = self.state
        if self.scorefun is not None:
            return self.scorefun(obj)
        else:
            return obj.score()

    def step(self):
        self.temperature = self.schedule(self.stepi)
        self.stepi += 1

        proposal, accepted = self.moves(self.state)
        diff = self.score(proposal) - self.score(self.state)
        if random.random() < np.exp(-diff/self.temperature):
            self.state = proposal
            accepted(True)
            return True
        else:
            accepted(False)
            return False

    def skip(self, n):
        acc = 0
        for _ in range(n):
            if self.step():
                acc += 1
        return acc

    def __iter__(self):
        while True:
            self.skip(self.K)
            yield self.state


class ReplicaExchange:
    """ Replica-Exchange MCMC
    """
    def __init__(self, initial, moves, temperatures, score):
        self.N = len(temperatures)
        self.temperatures = temperatures
        self.score = score
        self.moves = [copy(moves) for n in range(self.N)]
        self.states = [copy(initial) for n in range(self.N)]
        self.scores = [score(initial) for n in range(self.N)]
        self.best_score = score(initial)
        self.best_state = initial

    def step_replicas(self):
        """ For each replica, make and accept/reject one proposal. """
        for r in range(self.N):
            temperature = self.temperatures[r]
            state = self.states[r]
            moves = self.moves[r]
            score = self.scores[r]
            proposal, accepted = moves(state)
            propscore = self.score(proposal)
            if propscore < self.best_score:
                self.best_state = copy(proposal)
                self.best_score = propscore
            diff = propscore - score
            if random.random() < np.exp(-diff/temperature):
                self.states[r] = proposal
                self.scores[r] = propscore
                accepted(True)
            else:
                accepted(False)

    def _step1_exchange(self):
        if self.N < 2:
            return
        i1 = random.choice(range(self.N-1))
        i2 = i1+1
        E1 = self.scores[i1]
        E2 = self.scores[i2]
        T1 = self.temperatures[i1]
        T2 = self.temperatures[i2]
        diff = (E2/T1 + E1/T2) - (E1/T1 + E2/T2)
        if random.random() < np.exp(-diff):
            self.states[i1], self.states[i2] = self.states[i2], self.states[i1]
            self.scores[i1], self.scores[i2] = self.scores[i2], self.scores[i1]

    def step_exchange(self):
        for _ in range(self.N * self.N):
            self._step1_exchange()

    def step(self):
        self.step_replicas()
        self.step_exchange()

    def __iter__(self):
        while True:
            self.step()
            yield self.states[0]



class MPIReplicaExchange:
    """ Replica-Exchange MCMC running on OpenMPI
    """
    def __init__(self, init, moves, temperatures, score, K=10):
        from mpi4py import MPI
        self.MPI = MPI
        self.N = len(temperatures)
        self.temperatures = temperatures
        self.score = score
        self.moves = moves
        self.states = [copy(initial) for n in range(self.N)]


GATESETS = {
    'minimal': {IGate, OneQubitGate, CXGate},
    'minimal-discrete': {IGate, HadamardGate, Pi8Gate, Pi8InvGate, CXGate},
    'discrete': {IGate,
                 XGate, YGate, ZGate,
                 HadamardGate,
                 Pi8Gate, Pi8InvGate,
                 CXGate, CYGate, CZGate,
                 },
    'continuous': {IGate, OneQubitGate, RZZGate},
}

if __name__ == '__main__':
    import argparse
    import sys

    # Parse command-line arguments.
    parser = argparse.ArgumentParser(
        description='Annealing Quantum Compiler',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
    parser.add_argument('Q', type=int, help='Number of qubits')
    parser.add_argument('G', type=int, help='Maximum number of gates')
    parser.add_argument('out', type=str, help='Output filename')
    parser.add_argument('-a', '--ancilla', type=int, default=0,
        help='Number of ancillary qubits to use')
    parser.add_argument('-r', '--replicas', type=int, default=20,
        help='Number of replicas to use. Disabled if 0')
    parser.add_argument('-t', '--Tmin', type=float, default=0.1,
        help='Minimum temperature of replicas')
    parser.add_argument('-T', '--Tmax', type=float, default=3.,
        help='Maximum temperature of replicas')
    parser.add_argument('--gateset', choices=GATESETS, default='minimal-discrete',
        help='Set a gateset')
    parser.add_argument('--enable', default=[], action='append', choices=GATES,
        help='Enable a gate')
    parser.add_argument('--disable', default=[], action='append', choices=GATES,
        help='Disable a gate')
    parser.add_argument('--threshold', default=1e-8, type=float,
        help='Termination threshold')
    parser.add_argument('--reduce', action='store_true', help='Work without gates')
    parser.add_argument('--mpi', action='store_true', help='Use OpenMPI')
    args = parser.parse_args()
    Q = args.Q
    G = args.G
    N = 2**Q
    threshold = args.threshold
    gates = GATESETS[args.gateset]
    # TODO make enable and disable take effect in order (with a custom action)
    gates = gates.union(GATES[g] for g in args.enable)
    gates = gates.difference(GATES[g] for g in args.disable)

    # Read goal unitary from standard input.
    goal = np.array([[complex(x) for x in l.split()] for l in sys.stdin.readlines()])
    # Validate size and unitarity.
    if goal.shape != (N, N):
        sys.stderr.write('Goal is wrong shape\n')
        sys.exit(1)
    if np.sum(abs(np.eye(N) - goal.T.conj() @ goal)) > 1e-8:
        sys.stderr.write('Goal is not unitary\n')
        sys.exit(1)

    # Add ancilla.
    Q = Q + args.ancilla
    for _ in range(args.ancilla):
        goal = np.kron(np.eye(2), goal)

    def write_circuit(circuit):
        with open(args.out, 'w') as f:
            f.write(str(circuit))

    if args.reduce:
        initial = Reduction(Q, G, goal)
        moves = ReductionMove(Q)
        def score(reduction):
            return reduction.score()
    else:
        initial = Circuit(Q, G)
        moves = CircuitMoveset(gates)
        def schedule(i):
            return 10 * np.exp(-i/40000)
        #def score(circuit):
        #    return dist(circuit.unitary(), goal)
        def score(circuit):
            #return dist_distinct_eigenvalues(circuit.unitary(), goal) + dist(circuit.unitary(), goal)/10.
            return dist_eigenvalue_multiplicities(circuit.unitary(), goal) + dist(circuit.unitary(), goal)/10.

    if args.replicas == 0:
        if args.mpi:
            sys.stderr.write('MPI not useful without replicas\n')
            sys.exit(1)
        try:
            mcmc = Annealing(initial, moves, schedule, score)
            best = mcmc.score()
            best_circuit = initial
            for circuit in mcmc:
                if mcmc.score() < threshold:
                    write_circuit(circuit)
                    break
                if mcmc.score() < best:
                    write_circuit(circuit)
                    print(mcmc.score())
                    best = mcmc.score()
                    best_circuit = circuit
                #print('==========')
                #print(circuit)
                #print(mcmc.temperature, mcmc.score(), len(circuit))
        except KeyboardInterrupt:
            print('Stopped. Writing circuit...')
            write_circuit(best_circuit)
            print('Circuit written!')
    else:
        if args.mpi:
            pass
        else:
            try:
                Ts = list(np.exp(np.linspace(np.log(args.Tmin), np.log(args.Tmax), args.replicas)))
                mcmc = ReplicaExchange(initial, moves, Ts, score)
                for circuit in mcmc:
                    write_circuit(mcmc.best_state)
                    if mcmc.best_score < threshold:
                        print(F'Threshold reached: {mcmc.best_score}')
                        break
                    print(mcmc.best_score, mcmc.scores)
                    #print(circuit)
            except KeyboardInterrupt:
                print('Stopped. Writing circuit...')
            write_circuit(mcmc.best_state)
            print('Circuit written!')
